﻿using System;
using System.Collections.Generic;
using AdoNetConsoleApp.DataModel;
using AdoNetConsoleApp.DTO;
using AdoNetConsoleApp.DTOFactory;
using MySql.Data.MySqlClient;


namespace AdoNetConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            MySqlConnection connection = PrepareMySQLConection();
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            var sf = new SexFactory();
            var pf = new PersonFactory();
            SexModel sexModel = new SexModel(connection,command, sf.CreateDtoObject);
            var result=sexModel.ReadAll();
            ShowDataSet(result);
            PersonModel personModel = new PersonModel(connection,command, pf.CreateDtoObject);
            var result2=personModel.ReadAll();
            ShowDataSet(result2);
            connection.Close();
        }

        static MySqlConnection PrepareMySQLConection()
        {
            string connectionString = "server=localhost;uid=dotnet;pwd=12345;database=DEMO";
            //"server=127.0.0.1;uid=root;pwd=12345;database=test"
            MySqlConnection connection= new MySqlConnection();
            try
            {
                connection.ConnectionString = connectionString;
                connection.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return connection;
        }

        static void ShowDataSet(IEnumerable<IDtoObject> recordsset)
        {
            foreach (var record in recordsset)
            {
                Console.WriteLine(record.ToString());
            }
        }
    }
}
///home/guyver/RiderProjects/AdoNetConsoleApp_v2/AdoNetConsoleApp_v2/Program.cs