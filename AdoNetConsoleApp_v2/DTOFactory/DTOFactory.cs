using System.Data.Common;
using AdoNetConsoleApp.DTO;

namespace AdoNetConsoleApp.DTOFactory;

public abstract class DTOFactory
{
    public abstract IDtoObject CreateDtoObject(DbDataReader reader);
}