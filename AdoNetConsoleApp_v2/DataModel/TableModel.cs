using System;
using System.Collections.Generic;
using System.Data.Common; 
using AdoNetConsoleApp.DTO;
//using MySql.Data.MySqlClient;

namespace AdoNetConsoleApp.DataModel
{
    public abstract class TableModel:ICRUDModel
    {
        protected string tableName;
        protected DbConnection connection;
        protected DbCommand command;
        protected Func<DbDataReader,IDtoObject> factory;
        protected string selectQuery; 
        public string TableName
        {
            get { return tableName; }
        }
        public TableModel(string table,DbConnection connection,DbCommand command,Func<DbDataReader,IDtoObject> factory)
        {
            tableName = table;
            this.connection = connection;
            this.command = command;
            selectQuery = $"SELECT * FROM {tableName};";
            this.factory = factory;
        }

        public virtual IEnumerable<IDtoObject> ReadAll()
        {
            List<IDtoObject> result = new List<IDtoObject>();
            command.CommandText = selectQuery;
            using (DbDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.Add(factory(reader));
                }
            }
            return result;
        }
    }
}