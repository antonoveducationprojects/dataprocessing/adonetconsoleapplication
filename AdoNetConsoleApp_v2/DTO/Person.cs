using System;
namespace AdoNetConsoleApp.DTO
{
    public class Person : BaseDto, IDtoObject //???
    {
        public int PersonId { get; set; }
        public string Name { get; set; }
        public string Sname { get; set; }
        public int SexId { get; set; }
        public DateTime WhenBorn { get; set; }
        public override string ToString()
        {
            return $"{PersonId}\t{Name}\t{Sname}\t{SexId}\t{WhenBorn}";
        }
    }
}