namespace AdoNetConsoleApp.DTO
{
    public class Sex:BaseDto,IDtoObject   ///???
    {
        public int sex_id { get; set; }
        public string description { get; set; }
        public override string ToString()
        {
            return $"{sex_id}\t{description}";
        }
    }
}