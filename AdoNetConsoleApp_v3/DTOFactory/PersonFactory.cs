namespace AdoNetConsoleApp.DTOFactory;
using System.Data.Common;
using AdoNetConsoleApp.DTO;

public class PersonFactory:DTOFactory
{
    public override IDtoObject CreateDtoObject(DbDataReader reader)
    {
        return new Person()
        {
            PersonId =  reader.GetInt32(0),
            Name = reader.GetString(1),
            Sname = reader.GetString(2),
            SexId = reader.GetInt32(3),
            WhenBorn = reader.GetDateTime(4)
        };
    }
}