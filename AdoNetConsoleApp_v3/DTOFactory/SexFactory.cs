namespace AdoNetConsoleApp.DTOFactory;
using System.Data.Common;
using AdoNetConsoleApp.DTO;
public class SexFactory:DTOFactory
{
    public override IDtoObject CreateDtoObject(DbDataReader reader)
    {
        return new Sex() { sex_id = reader.GetInt32(0), description = reader.GetString(1) };
    }
}