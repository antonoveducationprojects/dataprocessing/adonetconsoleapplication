using System;
using System.Collections.Generic;
using System.Data.Common; 
using AdoNetConsoleApp.DTO;
//using MySql.Data.MySqlClient;

namespace AdoNetConsoleApp.DataModel
{
    public abstract class TableModel:ICRUDModel
    {
        protected string tableName;
        protected DbConnection connection;
        protected DbCommand command;
        protected Func<DbDataReader,IDtoObject> factory;
        protected string selectQuery; 
        public string TableName
        {
            get { return tableName; }
        }
        public TableModel(string table,DbConnection connection,DbCommand command,Func<DbDataReader,IDtoObject> factory)
        {
            tableName = table;
            this.connection = connection;
            this.command = command;
            selectQuery = $"SELECT * FROM {tableName};";
            this.factory = factory;
        }

        public virtual IEnumerable<IDtoObject> ReadAll()
        {
            List<IDtoObject> result = new List<IDtoObject>();
            command.CommandText = selectQuery;
            using (DbDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.Add(factory(reader));
                }
            }
            return result;
        }

        public virtual int ExecuteNonSelect(string query)
        {
            if (query == String.Empty)
                return 0;
            if (query.StartsWith("SELECT"))//RegExp ???
                return 0;
            //if null???
            command.CommandText = query;
            return command.ExecuteNonQuery();
            // UPDATE {table} SET col1=val1, col2=val2, col3=val, ... WHERE key_id={key_val}
            //throw new NotImplementedException();
        }
        
        public virtual int Update(/*BaseDto*/IDtoObject dataObject)
        {
            string updateQuery = $"UPDATE {tableName} SET {dataObject.ToUpdateQuery()} WHERE {dataObject.GetStringId()}";
            
            // UPDATE {table} SET col1=val1, col2=val2, col3=val3, ... [WHERE key_id={key_val}]
            
            //Form updateQuery
            return ExecuteNonSelect(updateQuery);
            //throw new NotImplementedException();
        }
    }
}