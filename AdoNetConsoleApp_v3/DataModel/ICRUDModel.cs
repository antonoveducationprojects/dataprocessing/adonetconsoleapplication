using System.Collections.Generic;
using AdoNetConsoleApp.DTO;
namespace AdoNetConsoleApp.DataModel
{
    public interface ICRUDModel
    {
        IEnumerable<IDtoObject> ReadAll();

        int ExecuteNonSelect(string query);

        int Update(/*BaseDto*/IDtoObject dataObject);
        /*
          int Create(string query);
          int Delete(string query);
        */
    }
}