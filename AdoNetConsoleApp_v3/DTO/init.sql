/*CREATE TABLE sex
(
    sex_id int unsigned not null auto_increment,
    description VARCHAR(20),
    PRIMARY KEY(sex_id)
) ENGINE=InnoDB;
*/
CREATE TABLE person
(
    PersonId int unsigned not null auto_increment,
    Name CHAR(50) NOT NULL,
    Sname CHAR(50) NOT NULL,
    SexId INT UNSIGNED NOT NULL,
    WhenBorn DateTime NOT NULL,
    PRIMARY KEY(PersonId),
    Foreign KEY(SexId) REFERENCES sex(sex_id) ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB;