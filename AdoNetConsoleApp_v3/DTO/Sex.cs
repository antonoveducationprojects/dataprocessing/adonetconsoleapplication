namespace AdoNetConsoleApp.DTO
{
    public class Sex:BaseDto,IDtoObject   ///???
    {
        public int sex_id { get; set; }
        public string description { get; set; }
        public override string ToString()
        {
            return $"{sex_id}\t{description}";
        }

        public int GetId()
        {
            return sex_id;
        }
        
        public string GetStringId()
        {
            return $"sex_id={sex_id}";
        }       

        public string ToInsertQuery()
        {
            string result = $"(description) VALUES(\"{description}\")";
            return result;
        }

        public string ToUpdateQuery()
        {
            string result = $"description=\"{description}\"";
            return result;
        }
    }
}