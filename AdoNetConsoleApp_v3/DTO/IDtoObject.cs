namespace AdoNetConsoleApp.DTO
{
    public interface IDtoObject
    {
        string ToString();
        string ToUpdateQuery();  //???
        string ToInsertQuery();  //???
        int GetId(); //???
        string GetStringId();
    }
}