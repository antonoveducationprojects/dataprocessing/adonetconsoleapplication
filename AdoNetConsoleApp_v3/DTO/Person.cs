using System;
namespace AdoNetConsoleApp.DTO
{
    public class Person : BaseDto, IDtoObject //???
    {
        public int PersonId { get; set; }
        public string Name { get; set; }
        public string Sname { get; set; }
        public int SexId { get; set; }
        public DateTime WhenBorn { get; set; }
        public override string ToString()
        {
            return $"{PersonId}\t{Name}\t{Sname}\t{SexId}\t{WhenBorn}";
        }

        public int GetId()
        {
            return PersonId;
        }
        
        public string GetStringId()
        {
            return $"PersonId={PersonId}";
        }

        public string ToInsertQuery()
        {
            string result = $"(Name,Sname,SexId,WhenBorn) VALUES(\"{Name},{Sname},{SexId},{WhenBorn}\")";
            return result;
        }

        public string ToUpdateQuery()
        {
            var date = WhenBorn.Date;
            string sdate = $"{date.Year}-{date.Month}-{date.Day}";
            
            string result = $"Name=\"{Name}\", Sname=\"{Sname}\",SexId=\"{SexId}\",WhenBorn=\"{sdate}\"";
            return result;
        }
    }
}