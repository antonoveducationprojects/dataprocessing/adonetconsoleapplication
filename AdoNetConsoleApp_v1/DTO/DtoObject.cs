namespace AdoNetConsoleApp.DTO
{
    public interface IDtoObject
    {
        string ToString();
    }
}