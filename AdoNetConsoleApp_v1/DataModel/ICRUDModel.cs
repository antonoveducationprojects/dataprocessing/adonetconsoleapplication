using System.Collections.Generic;
using AdoNetConsoleApp.DTO;
namespace AdoNetConsoleApp.DataModel
{
    public interface ICRUDModel
    {
        IEnumerable<IDtoObject> ReadAll();
        /*
        int Update(string query);
        int Create(string query);
        int Delete(string query);
        */
    }
}