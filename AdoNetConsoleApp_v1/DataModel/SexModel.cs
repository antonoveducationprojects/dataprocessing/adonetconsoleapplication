using System;
using System.Collections.Generic;
using AdoNetConsoleApp.DTO;
using MySql.Data.MySqlClient;

namespace AdoNetConsoleApp.DataModel
{
    public class SexModel:TableModel
    {

        public SexModel(MySqlConnection connection) : base("sex", connection)
        {
            
        }

        public override IEnumerable<IDtoObject> ReadAll()
        {
            string query = $"SELECT * FROM {tableName};";
            List<IDtoObject> result = new List<IDtoObject>();
            MySqlCommand command = new MySqlCommand(query, connection);
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.Add(new Sex()
                        {
                            sex_id = reader.GetInt32(0),
                            description = reader.GetString(1)
                        });
                }
            }
            return result;
        }
    }
}