using System;
using System.Collections.Generic;
using AdoNetConsoleApp.DTO;
using MySql.Data.MySqlClient;

namespace AdoNetConsoleApp.DataModel
{
    public class PersonModel:TableModel
    {
        public PersonModel(MySqlConnection connection) : base("person", connection)
        {
            
        }

        public override IEnumerable<IDtoObject> ReadAll()
        {
            string query = $"SELECT * FROM {tableName};";
            List<IDtoObject> result = new List<IDtoObject>();
            //System.Data.DataTable table = connection.GetSchema("shema name");
            MySqlCommand command = new MySqlCommand(query, connection);
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.Add(new Person()
                    {
                        PersonId =  reader.GetInt32(0),
                        Name = reader.GetString(1),
                        Sname = reader.GetString(2),
                        SexId = reader.GetInt32(3),
                        WhenBorn = reader.GetDateTime(4)
                    });
                }
            }
            return result;
        }
    }
}