using System.Collections.Generic;
using System.ComponentModel;
using AdoNetConsoleApp.DTO;
using MySql.Data.MySqlClient;

namespace AdoNetConsoleApp.DataModel
{
    public abstract class TableModel:ICRUDModel
    {
        protected string tableName;
        protected MySqlConnection connection;
        public string TableName
        {
            get { return tableName; }
        }
        public TableModel(string table,MySqlConnection connection)
        {
            tableName = table;
            this.connection = connection;
        }

        public abstract IEnumerable<IDtoObject> ReadAll();
    }
}