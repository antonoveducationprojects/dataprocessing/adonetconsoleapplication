﻿using System;
using System.Collections.Generic;
using AdoNetConsoleApp.DataModel;
using AdoNetConsoleApp.DTO;
using MySql.Data.MySqlClient;


namespace AdoNetConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            MySqlConnection connection = PrepareMySQLConection();
            SexModel sexModel = new SexModel(connection);
            var result=sexModel.ReadAll();
            ShowDataSet(result);
            PersonModel personModel = new PersonModel(connection);
            var result2=personModel.ReadAll();
            ShowDataSet(result2);
            connection.Close();
        }

        static MySqlConnection PrepareMySQLConection()
        {
            string connectionString = "server=localhost;uid=dotnet;pwd=12345;database=DEMO";
            //"server=127.0.0.1;uid=root;pwd=12345;database=test"
            MySqlConnection connection= new MySqlConnection();
            try
            {
                connection.ConnectionString = connectionString;
                connection.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }

            return connection;
        }

        static void ShowDataSet(IEnumerable<IDtoObject> recordsset)
        {
            foreach (var record in recordsset)
            {
                Console.WriteLine(record.ToString());
            }
        }
    }
}